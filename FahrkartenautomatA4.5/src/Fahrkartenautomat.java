﻿import java.text.DecimalFormat;
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	

    	double betrag;
    	double rückgabe;
    	betrag = fahrkartenbestellungErfassen();
    	rückgabe = fahrkartenBezahlen(betrag);
    	fahrkartenAusgeben();
    	rueckgeldAusgeben(rückgabe);
    
    }
    
    
    public static double fahrkartenbestellungErfassen() {
    	double[] ticketPreis = new double[10]; 
    	ticketPreis[0] = 2.90;
    	ticketPreis[1] = 3.30; 
    	ticketPreis[2] = 3.60; 
    	ticketPreis[3] = 1.90;
    	ticketPreis[4] = 8.60;
    	ticketPreis[5] = 9.00;
    	ticketPreis[6] = 9.60;
    	ticketPreis[7] = 23.50;
    	ticketPreis[8] = 24.30;
    	ticketPreis[9] = 24.9;
    	
    	String[] ticketBezeichnung = new String[10];
    	ticketBezeichnung[0] = "Einzelfahrschein Berlin AB";
    	ticketBezeichnung[1] = "Einzelfahrschein Berlin BC"; 
    	ticketBezeichnung[2] = "Einzelfahrschein Berlin ABC"; 
    	ticketBezeichnung[3] = "Kurzstrecke";
    	ticketBezeichnung[4] = "Tageskarte Berlin AB";
    	ticketBezeichnung[5] = "Tageskarte Berlin BC";
    	ticketBezeichnung[6] = "Tageskarte Berlin ABC";
    	ticketBezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
    	ticketBezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";
    	ticketBezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";
    	   
    	System.out.println("Unser Fahrkartenangebot:");
    	for(int i = 0; i <= 9; i++) {
    		System.out.println(format(ticketPreis[i]) + "€\t" + ticketBezeichnung[i] + " (" + i + ")");
    	}
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin aus:"); 
    	
    	Scanner tastatur = new Scanner(System.in);
    	int eingabeAuswahl;
    	eingabeAuswahl = tastatur.nextInt();
    	double zuZahlenderBetrag; 
        double anzFahrkarten;
        double preis = 0;
        int x = -1;
        
        
        for (int i = 0; i <= 9; i++) {
        	if(eingabeAuswahl == i) {
        		x = i;
        		if(x >= 0 && x <= 9) {
            		System.out.println("Ihre Wahl: " + ticketBezeichnung[x]);
            		preis = ticketPreis[x];
            	}else{
            		System.out.println(">>falsche Eingabe<<");
                	eingabeAuswahl = tastatur.nextInt();
            	}
        	}
        }
        
        System.out.println("Zu zahlender Betrag (EURO): " + format(preis));
        zuZahlenderBetrag = preis;
        preis = 0; 
        System.out.print("Anzahl der Tickets: ");
        anzFahrkarten = tastatur.nextDouble();
        zuZahlenderBetrag *= anzFahrkarten;
        
        return zuZahlenderBetrag;
        
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	double eingeworfeneMünze;
    	double rückgabebetrag;
    	double eingezahlterGesamtbetrag = 0.0;
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.println("Noch zu zahlen: " + format((zuZahlenderBetrag - eingezahlterGesamtbetrag)) + " Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        rueckgeldAusgeben(rückgabebetrag);
        return rückgabebetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + format(rückgabebetrag) + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
    	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static String format(double i) { //Runden
    	DecimalFormat f = new DecimalFormat("#0.00");
    	double toFormat = ((double)Math.round(i*100))/100;
    	return f.format(toFormat);
    }
    
}