import java.util.Scanner;

public class KlausurVogtland {
	
	public static void main(String[] args) {        
       programmhinweis();
       double eingabe = eingabe();
       verarbeitung(eingabe);

	}
	
	public static void programmhinweis() {
		//1.Programmhinweis
        System.out.println("Hinweis: ");
        System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");
	}
	
	public static double eingabe(double zahl1, double zahl2) {
		Scanner myScanner = new Scanner(System.in);
		//4.Eingabe
        System.out.print(" 1. Zahl: ");
        zahl1 = myScanner.nextDouble();
        System.out.print(" 2. Zahl: ");
        zahl2 = myScanner.nextDouble();
        
        return(zahl1, zahl2);
	}
	
	public static double verarbeitung(double zahl1, double zahl2, double erg) {
		//3.Verarbeitung
		
        erg = zahl1 * zahl2;
	}
	
	public static void ausgabe() {
		//2.Ausgabe   
        System.out.println("Ergebnis der Multiplikation: ");
        System.out.printf("%.2f * %.2f = %.2f%n", zahl1, zahl2, erg);
	}
	
	
}
	