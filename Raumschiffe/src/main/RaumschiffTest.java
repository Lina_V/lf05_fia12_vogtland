package main;

import java.util.ArrayList;
import java.util.Scanner;

public class RaumschiffTest {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		//photonenAnz, energieversorgung, schild, h�lle, lebenserh.syst., androiden, name
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		
		//bezeichnung, menge
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Borg-Schrott", 5);
		Ladung l3 = new Ladung("Rote Materie", 2);
		Ladung l4 = new Ladung("Forschungssonde", 35);
		Ladung l5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung l6 = new Ladung("Plasma-Waffe", 50);
		Ladung l7 = new Ladung("Photonenthorpedo", 3);
		
		klingonen.addLadung(l1);
		klingonen.addLadung(l5);
		
		romulaner.addLadung(l2);
		romulaner.addLadung(l3);
		romulaner.addLadung(l6);
		
		vulkanier.addLadung(l4);
		vulkanier.addLadung(l7);
		
//		System.out.println("Test Zustand");
//		klingonen.zustandRaumschiff();
//		romulaner.zustandRaumschiff();
//		vulkanier.zustandRaumschiff();
//		
//		System.out.println("Test Ladung ausgeben");
//		klingonen.ladungsverzeichnisAusgeben();
//		romulaner.ladungsverzeichnisAusgeben();
//		vulkanier.ladungsverzeichnisAusgeben();
//		
//		System.out.println("Test schie�en Photon");
//		romulaner.photonenthorpedoSchiessen(vulkanier);
//		romulaner.photonenthorpedoSchiessen(vulkanier);
//		
//		System.out.println("Test schie�en Phaser");
//		romulaner.phaserkanoneSchiessen(vulkanier);
//		romulaner.phaserkanoneSchiessen(vulkanier);
//		
//		System.out.println("Test Logbuch");
//		ArrayList<String> ergebnis= romulaner.eingetrageneLogbuchZurueckgeben();
//		System.out.println(ergebnis);
		
		vulkanier.photonentorpedosLaden(1);
		//klingonen.photonentorpedosLaden(2);
		
	}

}
