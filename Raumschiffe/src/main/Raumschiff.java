package main;

import java.util.ArrayList;

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	private ArrayList<String> logbuch = new ArrayList<String>();
	
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int andoidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.schiffsname = schiffsname;
		this.androidenAnzahl = andoidenAnzahl;
		
	}


	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}


	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}


	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}


	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}


	public int getSchildeInProzent() {
		return schildeInProzent;
	}


	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}


	public int getHuelleInProzent() {
		return huelleInProzent;
	}


	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}


	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}


	public void setLebenserhaltungssystemInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}


	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}


	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}


	public String getSchiffsname() {
		return schiffsname;
	}


	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	public void photonenthorpedoSchiessen(Raumschiff r) {
		int photonenAnz = getPhotonentorpedoAnzahl();
		Raumschiff gegner = r;
		
		if(photonenAnz >= 1) {
			photonenAnz -= 1;
			setPhotonentorpedoAnzahl(photonenAnz);
			nachichtAnAlle("Photonentorpedo abgeschossen!");
			treffer(r);
		}else {
			nachichtAnAlle("-=*Click*=-");
		}
	}
	
	public void phaserkanoneSchiessen(Raumschiff r) {
		int energie = getEnergieversorgungInProzent();
		if(energie > 50) {
			energie -= 50;
			setEnergieversorgungInProzent(energie);
			nachichtAnAlle("Phaserkanone abgeschossen!");
			treffer(r);
		}else {
			nachichtAnAlle("-=*Click*=-");
		}
	}
	
	private void treffer(Raumschiff r) {
		int schild = r.schildeInProzent;
		int h�lle = r.huelleInProzent;
		int energie = r.energieversorgungInProzent;
		int leben = r.lebenserhaltungssystemeInProzent;
		
		nachichtAnAlle(r.schiffsname + " wurde getroffen.");
		
		schild -= 50;
		
		r.setSchildeInProzent(schild);
		System.out.println("s" + schild);
		if(r.schildeInProzent <= 0) {
			h�lle -= 50;
			energie -= 50;

			System.out.println("h" + h�lle);
			System.out.println("e" + energie);
			r.setHuelleInProzent(h�lle);
			r.setEnergieversorgungInProzent(energie);
			if(r.huelleInProzent <= 0) {
				leben = 0;
				r.setLebenserhaltungssystemInProzent(leben);
				nachichtAnAlle("Lebenserhaltungssysteme vernichtet.");
			}
		}
		
	}
	
	public void nachichtAnAlle(String message) {
		System.out.print("An alle: ");
		this.broadcastKommunikator.add(message);
		System.out.println(message);
		leerzeichen();
	}
	
	public ArrayList<String> eingetrageneLogbuchZurueckgeben(){
		
		return this.broadcastKommunikator;
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		int anzTorpedoLaden = anzahlTorpedos;
		int anBoard = 0;
		boolean istVorhanden = false;
		
		System.out.println(this.getSchiffsname());
		
		for(int i = 0; i < this.ladungsverzeichnis.size(); i++) {
			//String ladung = this.ladungsverzeichnis.get(i).getBezeichnung();
			if(this.ladungsverzeichnis.get(i).getBezeichnung().equals("Photonenthorpedo")) {
				anBoard = this.ladungsverzeichnis.get(i).getMenge();
				System.out.println(anBoard + " Torpedos vorhanden");
				istVorhanden = true;
			}
		}
		if(istVorhanden == false) {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachichtAnAlle("-=*Click*=-");
			leerzeichen();
		}else if(istVorhanden == true && anBoard < anzahlTorpedos) {
			System.out.println(anBoard + " Torpedos vorhanden...");
			this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl());
		}else {
			
		}
		leerzeichen();
		
	}
	
	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlAndroiden) {
		
	}
	
	public void zustandRaumschiff() {
		
		String name = getSchiffsname();
		int photonAnz = getPhotonentorpedoAnzahl();
		int energie = getEnergieversorgungInProzent();
		int schild = getSchildeInProzent();
		int huelle = getHuelleInProzent();
		int leben = getLebenserhaltungssystemeInProzent();
		int androiden = getAndroidenAnzahl();
		
		System.out.println("Name: " + name);
		System.out.println("Anzahl Photonenthorpedos: " + photonAnz);
		System.out.println("Energieversorung: " + energie + "%");
		System.out.println("Schild: " + schild + "%");
		System.out.println("H�lle: " + huelle + "%");
		System.out.println("Lebenserhaltungssystem: " + leben + "%");
		System.out.println("Anzahl Androiden: " + androiden);
		leerzeichen();
				
	}
	
	public void ladungsverzeichnisAusgeben() {
		for (int i = 0; i < this.ladungsverzeichnis.size(); i++){
			System.out.println(this.ladungsverzeichnis.get(i).getBezeichnung() + " " + this.ladungsverzeichnis.get(i).getMenge());
		}
		leerzeichen();
		
	}
	
	public void ladungsverzeichnisAufraeumen() {
		this.ladungsverzeichnis.clear();
	}
	
	public void leerzeichen() {
		System.out.println("");
	}

}
