﻿import java.text.DecimalFormat;
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	boolean run = true;
    	
    	while(run) {
    		double betrag;
        	double rückgabe;
        	betrag = fahrkartenbestellungErfassen();
        	rückgabe = fahrkartenBezahlen(betrag);
        	fahrkartenAusgeben();
        	//rueckgeldAusgeben(rückgabe);
    	}
    }
    
    public static void leerzeile(){
    	System.out.println(" ");
    }
    
    
    public static double fahrkartenbestellungErfassen() {
    	double[] ticketPreis = new double[10]; 
    	ticketPreis[0] = 2.90;
    	ticketPreis[1] = 3.30; 
    	ticketPreis[2] = 3.60; 
    	ticketPreis[3] = 1.90;
    	ticketPreis[4] = 8.60;
    	ticketPreis[5] = 9.00;
    	ticketPreis[6] = 9.60;
    	ticketPreis[7] = 23.50;
    	ticketPreis[8] = 24.30;
    	ticketPreis[9] = 24.9;
    	
    	String[] ticketBezeichnung = new String[10];
    	ticketBezeichnung[0] = "Einzelfahrschein Berlin AB";
    	ticketBezeichnung[1] = "Einzelfahrschein Berlin BC"; 
    	ticketBezeichnung[2] = "Einzelfahrschein Berlin ABC"; 
    	ticketBezeichnung[3] = "Kurzstrecke";
    	ticketBezeichnung[4] = "Tageskarte Berlin AB";
    	ticketBezeichnung[5] = "Tageskarte Berlin BC";
    	ticketBezeichnung[6] = "Tageskarte Berlin ABC";
    	ticketBezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
    	ticketBezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";
    	ticketBezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";
    	   
    	System.out.println("Unser Fahrkartenangebot:");
    	for(int i = 0; i <= 9; i++) {
    		System.out.println(format(ticketPreis[i]) + "€\t" + ticketBezeichnung[i] + " (" + i + ")");
    	}
    	
    	leerzeile();
    	
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin aus:"); 
    	
    	Scanner tastatur = new Scanner(System.in);
    	int eingabeAuswahl;
    	eingabeAuswahl = tastatur.nextInt();
    	double zuZahlenderBetrag; 
        double anzFahrkarten;
        double preis = 0;
        int x = -1;
        
        
        for (int i = 0; i <= 9; i++) {
        	if(eingabeAuswahl == i) {
        		x = i;
        		if(x >= 0 && x <= 9) {
            		System.out.println("Ihre Wahl: " + ticketBezeichnung[x]);
            		preis = ticketPreis[x];
            	}else{
            		System.out.println(">>falsche Eingabe<<");
                	eingabeAuswahl = tastatur.nextInt();
            	}
        	}
        }
        
        leerzeile();
        
        System.out.println("Zu zahlender Betrag (EURO): " + format(preis));
        zuZahlenderBetrag = preis;
        preis = 0; 
        
        leerzeile();
        
        System.out.print("Anzahl der Tickets: ");
        anzFahrkarten = tastatur.nextDouble();
        zuZahlenderBetrag *= anzFahrkarten;
        
        return zuZahlenderBetrag;
        
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	double eingeworfeneMünze;
    	double rückgabebetrag;
    	double eingezahlterGesamtbetrag = 0.0;
    	
    	System.out.println("Eingabe (5Ct, 10Ct, 20Ct, 50Ct, 1€, 2€, 5€, 10€ und 20€): ");
    	
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag){
     	   System.out.println("Noch zu zahlen: " + format((zuZahlenderBetrag - eingezahlterGesamtbetrag)) + " Euro");
     	   
     	   leerzeile();
     	   
     	   eingeworfeneMünze = tastatur.nextDouble();
     	   if(eingeworfeneMünze <= 20) {
     		   eingezahlterGesamtbetrag += eingeworfeneMünze;
     	   }else {
     		   System.out.println("Geld nicht erkannt, bitte mind. 5 Cent und Max. 20€!");
     	   }
        }
    	
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        rueckgeldAusgeben(rückgabebetrag);
        return rückgabebetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
           System.out.print("=");
           try {
 			Thread.sleep(250);
           } catch (InterruptedException e) {
 			e.printStackTrace();
           }
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0) {
    		leerzeile();
    		leerzeile();
     	    System.out.println("Der Rückgabebetrag in Höhe von " + format(rückgabebetrag) + " EURO");
     	    System.out.println("wird in folgenden Münzen ausgezahlt:");
     	   
     	    rückgeldMünzen(rückgabebetrag); 
        }
    	
    	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    	
    	leerzeile();
    }
    
    public static String format(double i) { 
    	DecimalFormat f = new DecimalFormat("#0.00");
    	double toFormat = ((double)Math.round(i*100))/100;
    	return f.format(toFormat);
    }
    
    public static double rückgeldMünzen(double rück) {
    	while(rück >= 20.0){
    		geldDarstellung(20.0);
    		System.out.println(rück);
     	    rück -= 20.0;
    	} 
    	while(rück >= 10.0){
     	    geldDarstellung(10.0);
     	   System.out.println(rück);
     	    rück -= 10.0;
    	}
    	while(rück >= 5.0){
    		geldDarstellung(5.0);
    		System.out.println(rück);
   	        rück -= 5.0;
    	}
    	while(rück >= 2.0){
    		geldDarstellung(2.0);
    		System.out.println(rück);
	        rück -= 2.0;
        }
        while(rück >= 1.0){
        	geldDarstellung(1.0);
        	System.out.println(rück);
	        rück -= 1.0;
        }
        while(rück >= 0.5){
        	geldDarstellung(0.50);
        	System.out.println(rück);
	        rück -= 0.5;
        }
        while(rück >= 0.2){
        	geldDarstellung(0.20);
        	System.out.println(rück);
	        rück -= 0.2;
         }
         while(rück >= 0.1){
        	 geldDarstellung(0.10);
        	 System.out.println(rück);
	          rück -= 0.1;
         }
         while(rück >= 0.05){
        	 geldDarstellung(0.05);
        	 System.out.println(rück);
	         rück -= 0.05;
         }
         return rück;
    }
    
    public static void geldDarstellung(double geld) {
    	int geldLevel = 9;
    	if(geld == 0.05) {
    		geldLevel = 0;
    	}else if(geld == 0.10) {
    		geldLevel = 1;
    	}else if(geld == 0.20) {
    		geldLevel = 2;
    	}else if(geld == 0.50) {
    		geldLevel = 3;
    	}else if(geld == 1.0) {
    		geldLevel = 4;
    	}else if(geld == 2.0) {
    		geldLevel = 5;
    	}else if(geld == 5.0) {
    		geldLevel = 6;
    	}else if(geld == 10.0) {
    		geldLevel = 7;
    	}else if(geld == 20.0) {
    		geldLevel = 8;
    	}else {
    		geldLevel = 9;
    	}
    	int x = geldLevel;
    	//11 - / 21
    	//6 |
    	
    	char [] [] g1 = new char [6] [11]; //6 Zeilen und 11 Spalten
    	//Zeile 1
    	g1[0][3] = '*';
    	g1[0][5] = '*';
    	g1[0][7] = '*';
    	
    	//Zeile 2
    	g1[1][1] = '*';
    	g1[1][9] = '*';
    	
    	//Zeile 3
    	g1[2][0] = '*';
    	g1[2][5] = '5';
    	g1[2][6] = ' ';
    	g1[2][10] = '*';
    	
    	//Zeile 4
    	g1[3][0] = '*';
    	g1[3][4] = 'C';
    	g1[3][5] = 'E';
    	g1[3][6] = 'N';
    	g1[3][7] = 'T';
    	g1[3][10] = '*';
    	
    	//Zeile 5
    	g1[4][1] = '*';
    	g1[4][9] = '*';
    	
    	//Zeile 6
    	g1[5][3] = '*';
    	g1[5][5] = '*';
    	g1[5][7] = '*';
    	
    	switch(x) {
    	case 0:
    		//System.out.printf(g1[0][3] + g1[0][5] + g1[0][7] + "\n");
    		
    		System.out.println("   * * *   ");
         	System.out.println(" *       * ");
         	System.out.println("*    5    *");
         	System.out.println("*   CENT  *");
         	System.out.println(" *       * ");
         	System.out.println("   * * *   ");
         	
    		break;
    	case 1:
    		System.out.println("   * * *   ");
         	System.out.println(" *       * ");
         	System.out.println("*    10   *");
         	System.out.println("*   CENT  *");
         	System.out.println(" *       * ");
         	System.out.println("   * * *   ");
         	break;
    	case 2:
    		System.out.println("   * * *   ");
          	System.out.println(" *       * ");
          	System.out.println("*    20   *");
          	System.out.println("*   CENT  *");
          	System.out.println(" *       * ");
          	System.out.println("   * * *   ");
    		break;
    	case 3:
    		System.out.println("   * * *   ");
          	System.out.println(" *       * ");
          	System.out.println("*    50   *");
          	System.out.println("*   CENT  *");
          	System.out.println(" *       * ");
          	System.out.println("   * * *   ");
          	break;
    	case 4:
    		System.out.println("   * * *   ");
          	System.out.println(" *       * ");
          	System.out.println("*    1    *");
          	System.out.println("*   EURO  *");
          	System.out.println(" *       * ");
          	System.out.println("   * * *   ");
    		break;
    	case 5:
    		System.out.println("   * * *   ");
	      	System.out.println(" *       * ");
	      	System.out.println("*    2    *");
	      	System.out.println("*   EURO  *");
	      	System.out.println(" *       * ");
	      	System.out.println("   * * *   ");
	      	break;
    	case 6:
    		System.out.println("* * * * * * * * * * *");
   	      	System.out.println("*                   *");
   	      	System.out.println("*       5EURO       *");
   	      	System.out.println("*                   *");
   	      	System.out.println("*                   *");
   	      	System.out.println("* * * * * * * * * * *");
   	      	break;
    	case 7:
         	System.out.println("* * * * * * * * * * *");
     	    System.out.println("*                   *");
     	    System.out.println("*       10EURO      *");
     	    System.out.println("*                   *");
     	    System.out.println("*                   *");
     	    System.out.println("* * * * * * * * * * *");
     	    break;
    	case 8:
    		System.out.println("* * * * * * * * * * *");
     	    System.out.println("*                   *");
     	    System.out.println("*       20EURO      *");
     	    System.out.println("*                   *");
     	    System.out.println("*                   *");
     	    System.out.println("* * * * * * * * * * *");
    		break;
    	case 9:
    		System.out.println("Fehler!");
    		break;
    	}
    }
    
}